import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerHomeComponent } from './customer-home/customer-home.component';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomersComponent } from './customers/customers.component';

@NgModule({
  declarations: [CustomerHomeComponent, CustomersComponent],
  imports: [CommonModule, CustomerRoutingModule]
})
export class CustomerModule {}
