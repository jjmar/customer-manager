import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/Customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  customers: Customer[] = [
    {
      id: 0,
      firstName: 'John',
      lastName: 'Doe',
      gender: 'na',
      address: 'Fake Street 12 3',
      city: 'Coimbra',
      district: 'Coimbra'
    },
    {
      id: 1,
      firstName: 'Jane',
      lastName: 'Doe',
      gender: 'na',
      address: 'Fake Street 12 4',
      city: 'Coimbra',
      district: 'Coimbra'
    }
  ];

  constructor() {}

  ngOnInit() {}

  deleteCustomer(customer: Customer) {
    console.log(customer);
    this.customers = this.customers.filter(c => c.id !== customer.id);
  }

  addCustomer(customer: Customer) {
    console.log(this.customers.push(customer), customer);
  }
}
